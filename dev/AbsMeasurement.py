#!/usr/bin/env python
""" Abstract base class for measurements.

Holds generic information about measurements, for example workspace, dataset,
pdf, nuisance parameters, etc.
"""

from __future__ import absolute_import

import logging
import os

import ROOT

__author__ = "Stefan Gadatsch"
__credits__ = ["Stefan Gadatsch", "Andrea Gabrielli"]
__version__ = "0.1"
__maintainer__ = "Stefan Gadatsch"
__email__ = "stefan.gadatsch@cern.ch"

__all__ = [
    'AbsMeasurement'
]


FORMAT = '%(asctime)s - %(processName)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
existing_logger = logging.getLogger('x')


class AbsMeasurement(object):
    def __init__(self, name, path=None, wsname=None, mcname=None, dataname=None):
        if path is None:
            path = ""
        if wsname is None:
            wsname = ""
        if mcname is None:
            mcname = ""
        if dataname is None:
            dataname = ""

        self._name = name
        self._path = path
        self._wsname = wsname
        self._mcname = mcname
        self._dataname = dataname
        self._isinit = False
        self._file = ROOT.TFile
        self._workspace = ROOT.RooWorkspace
        self._modelconfig = ROOT.RooStats.ModelConfig
        self._pdf = ROOT.RooAbsPdf
        self._data = ROOT.RooAbsData
        self._pois = ROOT.RooArgSet
        self._nuis = ROOT.RooArgSet
        self._globs = ROOT.RooArgSet
        self._obs = ROOT.RooArgSet
        self._binnedLikelihood = False
        self._nrCores = 1
        self._ghostEvents = False

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def workspaceName(self):
        return self._wsname

    @workspaceName.setter
    def workspaceName(self, wsname):
        self._wsname = wsname

    @property
    def modelConfigName(self):
        return self._mcname

    @modelConfigName.setter
    def modelConfigName(self, mcname):
        self._mcname = mcname

    @property
    def dataName(self):
        return self._dataname

    @dataName.setter
    def dataName(self, dataname):
        self._dataname = dataname

    @property
    def workspace(self):
        return self._workspace

    @workspace.setter
    def workspace(self, workspace):
        self._workspace = workspace

    @property
    def nrCores(self):
        return self._nrCores

    @nrCores.setter
    def nrCores(self, nrCores):
        self._nrCores = nrCores

    @property
    def ghostEvents(self):
        return self._ghostEvents

    @ghostEvents.setter
    def ghostEvents(self, ghostEvents):
        self._ghostEvents = ghostEvents

    @property
    def nuis(self):
        return self._nuis

    @nuis.setter
    def nuis(self, nuis):
        self._nuis = nuis

    @property
    def obs(self):
        return self._obs

    @obs.setter
    def obs(self, obs):
        self._obs = obs

    @property
    def globs(self):
        return self._globs

    @globs.setter
    def globs(self, globs):
        self._globs = globs

    @property
    def binnedLikelihood(self):
        return self._binnedLikelihood

    @binnedLikelihood.setter
    def binnedLikelihood(self, binnedLikelihood):
        self._binnedLikelihood = binnedLikelihood

    def writeToFile(self, path):
        self._ws.writeToFile(path)
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Wrote workspace to " + path)
        existing_logger.info(msg)


if __name__ == '__main__':
    pass
