#!/usr/bin/env python
""" Correlation scheme.

Organization of correlation scheme.
"""

from __future__ import absolute_import

import collections
import logging
import os
import pandas as pd


__author__ = "Stefan Gadatsch"
__credits__ = ["Stefan Gadatsch", "Andrea Gabrielli"]
__version__ = "0.1"
__maintainer__ = "Stefan Gadatsch"
__email__ = "stefan.gadatsch@cern.ch"

__all__ = [
    'CorrelationScheme'
]


FORMAT = '%(asctime)s - %(processName)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
existing_logger = logging.getLogger('x')


class CorrelationScheme(object):
    def __init__(self, name, pois=None, autocorrelation=None):
        if pois is None:
            pois = ""
        if autocorrelation is None:
            autocorrelation = False

        self._name = name
        self._pois = pois
        self._autocorrelation = autocorrelation
        self._renamings = collections.defaultdict(lambda: collections.defaultdict(dict))

        if autocorrelation is True:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Parameters with the same name will be correlated automatically!")
            existing_logger.warning(msg)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def autoCorrelation(self):
        return self._autocorrelation

    @autoCorrelation.setter
    def autoCorrelation(self, autocorrelation):
        if autocorrelation is True:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Parameters with the same name will be correlated automatically!")
            existing_logger.warning(msg)
        self._autocorrelation = autocorrelation

    @property
    def ParametersOfInterest(self):
        return self._pois

    @ParametersOfInterest.setter
    def ParametersOfInterest(self, pois):
        self._pois = pois

    @property
    def renamings(self):
        return self._renamings

    @renamings.setter
    def renamings(self, renamings):
        self._renamings = renamings

    def CorrelateParameter(self, OldParameterNamePlusMeasurement, NewParameterName):
        measurements = OldParameterNamePlusMeasurement.split(",")
        for measurement in measurements:
            OldParameterNames = measurement.split("::")
            OldParameterName = OldParameterNames[1]
            MeasName = OldParameterNames[0]
            self.RenameParameter(MeasName, OldParameterName, NewParameterName)

    def RenameParameter(self, measurement, old, new):
        self._renamings[measurement][old]["new"] = new

    def Print(self):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Printing correlation scheme")
        existing_logger.info(msg)
        tmp = {}
        for measurement, val in self._renamings.iteritems():
            for old, tags in val.iteritems():
                for tag, val in tags.iteritems():
                    if tag is "new":
                        if val not in tmp.keys():
                            tmp[val] = {}
                        tmp[val][measurement] = 'x'

        df = pd.DataFrame(tmp).T
        df.fillna(' ', inplace=True)
        with pd.option_context('display.max_rows', len(df)):
            print(df)


if __name__ == '__main__':
    pass
